export const state = () => ({
  email: "",
  password: "",
  username: "",
  jwt: "",
  logged: false,
})

  // Armazena métodos para alterar dados (setters)
  // que não podem ser assíncronos
export const mutations = {
    SUBMIT(state, payload) {
      state.email = payload.email
      state.password = payload.password
      state.username = payload.username
      state.jwt = payload.jwt
      state.logged = payload.logged
    },
    LIMPAR(state){
      state.email = ""
      state.password = ""
      state.username = ""
      state.jwt = ""
      state.logged = false
    },
}

// Armazena métodos para alterar dados (setters)
// que podem ser assíncronos
export const actions = {
  async signUp(context, payload) {
    try{
      context.commit("LIMPAR")
      const res = await this.$axios.post('/register', payload)
      if(res.status === 201) {
        payload.jwt = res.data.accessToken
        payload.logged = true
        context.commit("SUBMIT", payload)
      }else if(res.status === 400){
        alert("E-mail already exists")
      }
    }catch (e){
      alert(e)
    }
  },
  async signIn(context, payload){
    try{
      context.commit("LIMPAR")
      const res = await this.$axios.post('/login', payload)
      if(res.status === 200){
        payload.jwt = res.data.accessToken
        payload.logged = true
        context.commit("SUBMIT", payload)
      }else if(res.status === 400) {
        alert("Cannot find user")
      }
    }catch (e){
      alert(e)
    }
  },
}

// json-server db.json -m ./node_modules/json-server-auth
